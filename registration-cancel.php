<?php

session_start();

require( '../../../wp-config.php' );
require_once './helpers.php';
require_once 'attendee-class.php';

check_protection();

$attendeeID = $_GET['attendeeID'];
$attendeeAuth = $_GET['attendeeAuth'];

$attendee = new Attendee();    
$attendee->cancelPayment($attendeeID,$paymentID);
$attendee->find($attendeeID);
destroy_protection();
header('Location:'.strtok($_SERVER['HTTP_REFERER'],'?').'?step=canceled');
