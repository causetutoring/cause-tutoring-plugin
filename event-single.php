<?php 

session_start();

use Carbon\Carbon;

$step = (isset($_GET['step'])) ? $_GET['step'] : 'registration';
define('PLUGIN_URL',"http://localhost/wordpress/wp-content/plugins/registration");

require_once "helpers.php";
require_once "registration-class.php";
require_once "alert-class.php";
require_once "price-class.php";
require_once "attendee-class.php";

set_protection();
?>
<style>
.box{
    padding: 3%;
}
</style>

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('templates/page-header'); ?>

    <div class="container default-container">
        <div class="container default-content">
            <div class="maintext">

                <?php Alert::printMessage(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header>
                        <?php the_content();?>
                        <div class="event_date">
                            <p><strong>Starts at:</strong> <time datetime="<?=get_post_meta( get_the_ID(), 'start_date',true );?>"><?=Carbon::parse(get_post_meta(get_the_ID(), 'start_date',true ))->toDayDateTimeString();?></time><br/>
                            <strong>Finishes at:</strong> <time datetime="<?=get_post_meta( get_the_ID(), 'end_date',true );?>"><?=Carbon::parse(get_post_meta( get_the_ID(), 'start_date',true ))->toDayDateTimeString();?></time></p>
                        </div>
                    </header>
                    <?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>    
                </article>

                <hr/>

                <?php $available_tickets =  intval(get_post_meta(  get_the_ID(), 'amount_of_tickets',true )) - count(Registration::getOnlyActive( get_the_ID())); ?>
                <?php if($available_tickets > 0): ?>

                    <!-- registration step 1 view -->
                    <?php if($step === 'registration' ): ?>
                        <?php include 'includes/registration-form.php' ; ?>
                    <?php endif; ?>
                    <!-- end registration step 1 -->

                    <!-- PayPal Form -->
                    <?php if($step === 'payment'): ?>
                        <?php include 'includes/payment.php' ; ?>
                    <?php endif; // end if step payment ?>
                    <!-- end Paypal -->

                    <!-- confirmation page -->
                    <?php if($step === 'confirmation'): ?>
                        <?php include 'includes/confirmation.php' ; ?>
                    <?php endif; ?>
                    <!-- end of confirmation page -->

                    <!-- canceled page -->
                    <?php if($step === 'canceled'): ?>
                        <?php include 'includes/canceled.php' ; ?>
                    <?php endif; ?>
                    <!-- end of canceled page -->

                <?php else: ?>
                        <h4 style="color:red;">There is no available tickets in this event. </h4>
                <?php endif;?>
            </div><!-- end of .maintext -->
        </div><!-- end of .default-content -->
    </div><!-- end of .default-container -->
<?php endwhile; endif; ?>

<?php edit_post_link(); ?>

<?php get_footer(); ?> 

