<?php

session_start();

require( '../../../wp-config.php' );
require_once './helpers.php';
require_once 'attendee-class.php';

check_protection();

$attendeeID = $_GET['attendeeID'];
$paymentID = $_GET['paymentID'];
$attendeeAuth = $_GET['attendeeAuth'];

// dd(Attendee::getAttendeeSession()['id'] .' - '.$_GET['attendeeID']);
if((int)Attendee::getAttendeeSession()['id'] === (int)$_GET['attendeeID']){
    $attendee = new Attendee();    
    $attendee->changePaymentStatus($attendeeID,$paymentID);
    $attendee->find($attendeeID);
    
    $to = $attendee->email;
    $subject = "You have been registered on {$post->title}";
    $body = "";
    $headers = array('Content-Type: text/html; charset=UTF-8');
        
    wp_mail( $to, $subject, $body, $headers );
    destroy_protection();
    header('Location:'.strtok($_SERVER['HTTP_REFERER'],'?').'?step=confirmation');
} else{
    die('Attendee ID posted is not the same.');
}


