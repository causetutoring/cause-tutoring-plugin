<?php


class Registration{

    public static function get($wp_post_id)
    {
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}event_registration WHERE wp_post_id = {$wp_post_id}", OBJECT );
        return $results;
    }

    public static function getOnlyActive($wp_post_id)
    {
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}event_registration WHERE wp_post_id = {$wp_post_id} AND payment_status <> 'canceled'", OBJECT );
        return $results;
    }



}