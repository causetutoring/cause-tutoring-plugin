<?php
/**
 * Handles prices items and table
 */
class Price{

    private  $prices;
    private  $wpdb;

    public function __construct(){
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    /**
     * Returns a list of price items related with price table
     *
     * @param int $event_price_table_id
     * @return void
     */
    public  function getAll($event_price_table_id)
    {
        $results = $this->wpdb->get_results( "SELECT * FROM {$this->wpdb->prefix}event_price_item WHERE event_price_table_id = $event_price_table_id", OBJECT );
        return $results;
    }    

    /**
     * Returns given price
     *
     * @param int $id
     * @return void
     */
    public static function get($id)
    {
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}event_price_item WHERE id = $id", OBJECT );
        return (double) $results[0]->price;        
    }

}