<?php
/**
 * Handles database actions in plugin activation process
 */
class DBFactory{

  public $prefix;
  public $wpdb;

  public function __construct($prefix,$wpdb){
    $this->prefix = $prefix;
    $this->wpdb = $wpdb;
  }

  public function createRegistration(){

    $sql = "CREATE TABLE IF NOT EXISTS `{$this->prefix}event_registration` (
      `id` INT NOT NULL AUTO_INCREMENT,
      `wp_post_ID` INT NULL COMMENT 'This is a event ID in wp_posts table',
      `fullname` VARCHAR(255) NULL,
      `email` VARCHAR(255) NULL,
      `association` VARCHAR(255) NULL,
      `faculty` VARCHAR(100) NULL,
      `type` VARCHAR(100) NULL,
      `payment_status` ENUM('paid','pending','canceled') NULL,
      `credit` DOUBLE NULL,
      `authorization` VARCHAR(255) NULL,
      `payment_id` VARCHAR(255) NULL,
      `registration_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
      `status` ENUM('active','canceled') NULL COMMENT 'Flag to set if user will attend to event or not',
      PRIMARY KEY (`id`))
    ENGINE = InnoDB;";

    $this->wpdb->query( $sql);

  }


  public function createPriceTable(){

    $sql = "
      CREATE TABLE IF NOT EXISTS `{$this->prefix}event_price_table` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(45) NULL,
        PRIMARY KEY (`id`))
      ENGINE = InnoDB
    ";

    $this->wpdb->query( $sql);

  }


  public function createPriceItems(){

    $sql = "
      CREATE TABLE IF NOT EXISTS `{$this->prefix}event_price_item` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `event_price_table_id` INT NULL,
        `slug` VARCHAR(45) NULL,
        `name` VARCHAR(45) NULL,
        `price` DOUBLE NULL,
        PRIMARY KEY (`id`))
      ENGINE = InnoDB
      COMMENT = '	'
    ";

    $this->wpdb->query( $sql);

  }


  public function populate(){

    $this->wpdb->insert( 
      "{$this->prefix}event_price_table", 
      array( 
        'name' => 'Standard Price Table' 
      ) 
    );


    $this->wpdb->insert( 
      "{$this->prefix}event_price_item", 
      array( 
        'event_price_table_id' => 1, 
        'slug' => "cause_member", 
        'name' => "Cause Member", 
        'price' => 5.00, 
      ) 
    );

    $this->wpdb->insert( 
      "{$this->prefix}event_price_item", 
      array( 
        'event_price_table_id' => 1, 
        'slug' => "student", 
        'name' => "Student", 
        'price' => 10.00, 
      ) 
    );

    $this->wpdb->insert( 
      "{$this->prefix}event_price_item", 
      array( 
        'event_price_table_id' => 1, 
        'slug' => "professor", 
        'name' => "Professor", 
        'price' => 20.00, 
      ) 
    );

    $this->wpdb->insert( 
      "{$this->prefix}event_price_item", 
      array( 
        'event_price_table_id' => 1, 
        'slug' => "professional", 
        'name' => "Work Professional", 
        'price' => 20.00, 
      ) 
    );

  }

  public function dropTables(){

    $sql = "DROP TABLE IF EXISTS `{$this->prefix}event_registration`";
    $this->wpdb->query($sql);
    $sql = "DROP TABLE IF EXISTS `{$this->prefix}event_price_table`";
    $this->wpdb->query($sql);
    $sql = "DROP TABLE IF EXISTS `{$this->prefix}event_price_item`";
    $this->wpdb->query($sql);
  }

 
}
