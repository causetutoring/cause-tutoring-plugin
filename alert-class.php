<?php

require_once 'helpers.php';

/**
 * Handles warnings after run methods  
 */
class Alert
{
    /**
     * Prints alert in the HTML page with Bootstrap 4 classes
     *
     * @return void
     */
    public static function printMessage()
    {
        $alert = get_session_flash('alert');

        if($alert !== null){
        
            $html = "<div class='alert alert-{$alert->context}'>";
            $html .= $alert->message;
            $html .= "</div>";
        
            echo $html;
        }
    }

    /**
     * Sets the message in a flash session method available in helpers.php
     * @uses helpers.php  
     *
     * @param [type] $context Context should receive context bootstrap classes 
     * @param [type] $message 
     * @return void
     */
    public static function set($context,$message)
    {

        $alertObject = new stdClass();
        $alertObject->context = $context;
        $alertObject->message = $message;

        set_session_flash('alert',$alertObject);
    }

}