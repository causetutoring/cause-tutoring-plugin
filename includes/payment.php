<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<?php
    $userID = Attendee::getAttendeeSession()['id'];
    $notpaid = Attendee::checkIfIsPaid($userID);
    $attendee = Attendee::find($userID);
   

?>
<?php if($notpaid): ;?>

    <div class="container text-center box">
        <div class="row">
            <div class="col">
                <h3>Your information has been saved!</h3>
                <p>Proceed with payment to complete your registration.</p> 
                <br/><br/>
            </div>
            <div class="col">
                <div id="paypal-button-container"></div>
            </div>
        </div>
    </div>
    <script>
        var URL = "<?=plugins_url();?>/ct-registration";
        var attendeeAuth = "<?=get_protection('session')?>";

        var attendeeID = "<?=Attendee::getAttendeeSession()['id']; ?>";
        var totalAmount = "<?=Attendee::getAttendeeSession()['credit'];?>";
        // Render the PayPal button

        paypal.Button.render({

            // Set your environment

            env: 'production', // sandbox | production

            // Specify the style of the button

            style: {
                label: 'pay',
                size:  'small', // small | medium | large | responsive
                shape: 'rect',   // pill | rect
                color: 'gold'   // gold | blue | silver | black
            },

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create

            client: {
                sandbox:    'AZ5MbanzQh1aVHsQCmETYm5YG7vItPhI38Z9N1YOjKNw0LK4t6HL0zs990c3zRglI8lNe1E3vMGCUeip',
                production: 'AXqLApIinn9EqTSz0NZSlHcDPyc2edBC97lBWcEyXe5l6g_xxnkKaVD8u1q-s7EZ-tWYJCRE-Wf5KSnG'
            },

            // Wait for the PayPal button to be clicked

            payment: function(data, actions) {

    
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { 
                                    total: "<?=Attendee::getAttendeeSession()['credit'];?>.00" , 
                                    currency: 'CAD'
                                },
                                description: "<?=$attendee->fullname?> (ID: <?=$attendee->id?>) [<?=$attendee->email?>] -- <?php the_title(); ?> (ID: <?php the_ID(); ?>)"
                            }
                        ]
                    }
                });
            },

            // Wait for the payment to be authorized by the customer

            onAuthorize: function(data, actions) {
                console.log(data)


                return actions.payment.execute().then(function() {

                    
                    var fetchURL = URL+'/registration-check.php?attendeeAuth='+attendeeAuth+'&attendeeID='+attendeeID+'&paymentID='+data.paymentID;
                    console.log(fetchURL);

                    window.location.href = fetchURL;
                });
            },

            onCancel: function(data, actions) {
                var fetchURL = URL+'/registration-cancel.php?attendeeAuth='+attendeeAuth+'&attendeeID='+attendeeID;
                window.location.href = fetchURL;
            }

        }, '#paypal-button-container');

    </script>
    

<?php else: ?>

    <div class="container text-center box">
        <div class="row">
            <div class="col">
                <h3> You already paid this event. </h3>
                <br/><br/>
            </div>
        </div>
    </div>
    
<?php endif; ?>