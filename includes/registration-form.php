<?php
    
    // require_once ("price-class.php");
    $prices = new Price($wpdb);
    $event_price_table_id = (int) get_post_meta( get_the_id(),'event_price_table', $single = true );
    // dd($event_price_table_id);
?>


    <h3>To register, please fill out this form.</h3>

    <div id="registration-form">

        <form method="POST" action="<?=plugins_url();?>/ct-registration/registration-store.php">
            
            <input type="hidden" name="event_id" value="<?php the_ID(); ?>" />
            <input type="hidden" name="protection" value="<?=get_protection('session') ?>" />

            <div class="form-group">
                <label>Full Name</label>
                <input  class="form-control" type="text" name="fullname" />
            </div>

            <div class="form-group">
                <label>E-mail</label>
                <input  class="form-control" type="email" name="email" />
            </div>

            <div class="form-group">
                <label>School, Employer or Association</label>
                <input class="form-control"  type="text" name="association" />
            </div>


            <div class="form-group">
                    <label>Field of study or work</label>
                    <input name="faculty" type="text" class="form-control"  />
                </div>
            </div>

            <div class="form-group">
                <label>Attending conference as:</label>
                <select  class="form-control" name="type" class="type">
                    <?php foreach($prices->getAll($event_price_table_id) as $price): ?>
                        <option  value="<?=$price->id;?>"> <?=$price->name ;?> $ <?=$price->price;?> CAD </option>
                    <?php endforeach; ?>    
                </select>
            </div>

            <button type="submit" class="btn btn-primary"> Proceed to Payment </button>
            
        </form>

        </div>
        
    </div>

    
