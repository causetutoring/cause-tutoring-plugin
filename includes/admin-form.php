<!-- 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
  
 -->




<link rel="stylesheet" href="<?=plugin_dir?>/ct-registration/assets/css/jquery-ui.css">
<script src="<?=plugin_dir?>/ct-registration/assets/js/jquery-ui.js"></script>
<script src="<?=plugin_dir?>/ct-registration/assets/js/form-validator.js"></script>
<?php


    use Carbon\Carbon;
    $post_status = get_post()->post_status; 
    $post_id = get_post()->ID;

    if(get_post_meta($post_id,'start_date',true) !== ""){
      $start_date = Carbon::parse(get_post_meta($post_id,'start_date',true));     
      
    }

    if(get_post_meta($post_id,'end_date',true) !== ""){
      $end_date = Carbon::parse(get_post_meta($post_id,'end_date',true));      
      // var_dump($end_date->minute);exit; 
    }

?>


<script>

$( function() {
  //initializing datepicker  
  $( "#start_date" ).datepicker({ minDate: -100,dateFormat: 'yy-mm-dd'});
  $( "#end_date" ).datepicker({ minDate: -100 ,dateFormat: 'yy-mm-dd'});

  //validate
  $.validate({
    lang: 'en',
    onError : function($form) {
      alert('Validation of form has failed!');
    },
  });

});
</script>

<?php if(get_post_meta($post_id,'start_date',true) !== ""): ?>
  <strong>Start Date: </strong><br/>
  <input placeholder="Event date. E.g.: 2017-01-01" data-validation="date" data-validation-format="yyyy-mm-dd" type='text' id="start_date" value='<?=$start_date->year;?>-<?=sprintf("%02d",$start_date->month);?>-<?=sprintf("%02d",$start_date->day);?>'  name='start_date[date]' />
  <input placeholder="20" data-validation="custom" data-validation-regexp="^([0][0-9]|1[0-9]|2[0-9]|3[0-1])$" type="text" min="0" max="23" value="<?=sprintf("%02d",$start_date->hour);?>" name="start_date[hour]" />
  :<input placeholder="30"  data-validation="custom" data-validation-regexp="^[0-5][0-9]$"  type="text" value="<?=sprintf("%02d",$start_date->minute);?>" name="start_date[minute]" min="0" max="59" />
  <br/>
<?php else: ?>
  <strong>Start Date: </strong><br/>
  <input placeholder="Event date. E.g.: 2017-01-01" data-validation="date" data-validation-format="yyyy-mm-dd" type='text' id="start_date"  name='start_date[date]' />
  <input placeholder="20"  data-validation="custom" data-validation-regexp="^([0][0-9]|1[0-9]|2[0-9]|3[0-1])$" type="text" min="0" max="23" name="start_date[hour]" />
  :<input placeholder="30" data-validation="custom" data-validation-regexp="^[0-5][0-9]$"  type="text" name="start_date[minute]" min="0" max="59" />
  <br/>
<?php endif; ?> 

<hr/>
<?php if(get_post_meta($post_id,'end_date',true) !== ""): ?>
  <strong>End Date: </strong><br/>
  <input placeholder="Event date. E.g.: 2017-01-01" data-validation="date" data-validation-format="yyyy-mm-dd" type='text' id="end_date"  value='<?=$end_date->year;?>-<?=sprintf("%02d",$end_date->month);?>-<?=sprintf("%02d",$end_date->day);?>' name='end_date[date]' />
  <input placeholder="20"  data-validation="custom" data-validation-regexp="^([0][0-9]|1[0-9]|2[0-9]|3[0-1])$"  type="text" min="0"  value="<?=sprintf("%02d",$end_date->hour);?>" name="end_date[hour]" max="23" />
  :<input placeholder="30"  data-validation="custom" data-validation-regexp="^[0-5][0-9]$"   value="<?=sprintf("%02d",$end_date->minute);?>" name="end_date[minute]" type="text" min="0" max="59" />
<?php else: ?>
  <strong>End Date: </strong><br/>
  <input placeholder="Event date. E.g.: 2017-01-01" data-validation="date" data-validation-format="yyyy-mm-dd" type='text' id="end_date"   name='end_date[date]' />
  <input placeholder="20"  data-validation="custom" data-validation-regexp="^^([0][0-9]|1[0-9]|2[0-9]|3[0-1])$"  type="text" min="0"  name="end_date[hour]" max="23" />
  :<input placeholder="30" data-validation="custom" data-validation-regexp="^[0-5][0-9]$"   name="end_date[minute]" type="text" min="0" max="59" />
<?php endif; ?>

<h3> Attendees</h3>
<table class="wp-list-table widefat fixed striped posts">
  <tr>
    <th> Full Name</th>
    <th> E-mail</th>
    <th> Association</th>
    <th> Faculty</th>
    <th> Type</th>
    <td> Payment Status</th>
    <td> Credit</th>
    <th> Registration Date</th>
    <th> Payment ID (Paypal)</th>
  </tr>

  <?php foreach(Registration::get($post_id) as $attendee): ?>
  <tr>
    <td> <?=$attendee->fullname; ?></td>
    <td> <?=$attendee->email; ?></td>
    <td> <?=$attendee->association; ?></td>
    <td> <?=$attendee->faculty; ?></td>
    <td> <?=$attendee->type; ?></td>
    <td> <?=$attendee->payment_status; ?></td>
    <td> <?=$attendee->credit; ?></td>
    <td> <?=$attendee->registration_date; ?></td>
    <td> <?=$attendee->payment_id; ?></td>
  </tr>
<?php endforeach;?>
</table>