<?php

use Carbon\Carbon;
/**
 * Handles list of events 
 */
class EventListController{
    /**
     * Returns a list of events
     *
     * @return array
     */
    public static function get()
    {
        $args = array(
            'posts_per_page'   => -1,
            'offset'           => 0,
            'order'            => 'ASC',
            'post_type'        => 'event',
            'post_status'      => 'publish',
            'orderby'			=> 'meta_value',
            'meta_key'			=> 'start_date',
            'suppress_filters' => true,     
            'meta_query'	=> array(
                'relation'		=> 'AND',
                array(
                    'key'			=> 'start_date',
                    'type'			=> 'DATETIME'
                )
            ) 
        );

        $posts = get_posts( $args );

        $postMap = array_map(function($v){
            return (object)[
                "ID"            =>  $v->ID,
                "post_title"    =>  $v->post_title,
                "post_content"  =>  $v->post_content,
                "start_date"    =>  Carbon::parse(get_post_meta( $v->ID, 'start_date',true ))->toDayDateTimeString(),
                "end_date"    =>  Carbon::parse(get_post_meta( $v->ID, 'end_date',true ))->toDayDateTimeString(),
                "guid"          => $v->guid
                            
            ];    
        },$posts);

        return $postMap;
        
    }
    
}