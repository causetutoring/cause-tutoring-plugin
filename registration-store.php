<?php

session_start();

require( '../../../'.'/wp-blog-header.php' );
require_once  './helpers.php' ;
require_once  './alert-class.php' ;
require_once  './attendee-class.php' ;
require_once  './price-class.php' ;

check_protection();


use Sirius\Validation\Validator;

$request = [
        'wp_post_ID'        =>  $_POST['event_id'],
        'fullname'          =>  $_POST['fullname'],
        'email'             =>  $_POST['email'],
        'association'       =>  $_POST['association'],
        'faculty'           =>  $_POST['faculty'],
        'type'              =>  $_POST['type'], 
        'credit'              =>  Price::get($_POST['type']), 
        'payment_status'    =>  'pending', 
        'authorization' =>  ''
];

//setting rules for validation
$validator = new Validator();
$validator->add([
        'fullname'                              => 'required | AlphaNumHyphen',
        'email'                                 => 'required | Email',
        'association'                           => 'required | AlphaNumHyphen',
        'faculty'                               => 'AlphaNumHyphen',
        'type'                                  => 'required | AlphaNumHyphen',
]);


//Check if attendee is already registered
$findAttendee = new Attendee(); 
$wasFound = count($findAttendee->findByEmail($_POST['email']));

if($wasFound){
        Alert::set('danger',"There is a attendee associated to <strong>{$_POST['email']}</strong> ");
        header('Location: ' . $_SERVER['HTTP_REFERER'] );
} else if($validator->validate($_POST)) {
        // Creating a new attendee instance and saving data 
        $attendee = new Attendee();
        $attendee->set($request);
        $attendeeID = $attendee->save();

        // Setting attendee info in session 
        Attendee::setAttendeeSession('id',$attendeeID);
        Attendee::setAttendeeSession('credit',$attendee->credit);
        // Attendee::setAuth();

        header('Location: ' . $_SERVER['HTTP_REFERER'] . "?step=payment");
    
} else {
        Alert::set('danger',"Inserted attendee information is not valid.");
        header('Location: ' . $_SERVER['HTTP_REFERER'] );
}

