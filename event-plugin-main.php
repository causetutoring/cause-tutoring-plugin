<?php
/*
Plugin Name: Cause Tutoring Registration
Plugin URI: 
Description: This is a plugin to list events and handles payment process. 
Author: Fabricio Biron 
Version: 1.0
Author URI: www.fabriciobiron.me
*/


require __DIR__ . '/vendor/autoload.php';
require 'carbon.php';
require 'registration-class.php';
require 'database-class.php';
require 'helpers.php';

use Carbon\Carbon;

define("plugin_dir", plugins_url());
 
/**
 * Plugin activation
 */
function ct_events_activate() 
{
    // Creating database
    global $wpdb;

    $db = new DBFactory($wpdb->prefix,$wpdb);
    $db->createRegistration();
    $db->createPriceTable();
    $db->createPriceItems();
    $db->populate();
    
    // Creating a Event Page and adding shortcode inside 
    $insert = wp_insert_post(array(
        'post_title' => "Cause Tutoring Events",
        'post_content' => '[ct_events_plugin_shortcode]',
        'post_status' => 'publish',
        'post_type'=>'page',
        'post_name'=>'ct-events'
    ));

    $inserted_post_id = $insert;
    add_option( 'ct_events_plugin_page_id', $inserted_post_id);
    
}

register_activation_hook( __FILE__, 'ct_events_activate' );
 

    
/**
 * Function to deactivate the plugin and
 * * Add delete created page for the plugin
 * * Retrives rows from plugin's tables, creates a json row and stores in wordpress options table
 * * Drops plugins tables
 * * Deletes plugin page
 * @return void
 */
function ct_events_deactivate() 
{
    global $wpdb;
    $now = serial_now(); 

    $event_registration_result = $wpdb->get_results("select * from {$wpdb->prefix}event_registration",ARRAY_N);
    $event_price_table_result = $wpdb->get_results("select * from {$wpdb->prefix}event_price_table",ARRAY_N);
    $event_price_item = $wpdb->get_results("select * from {$wpdb->prefix}event_price_item",ARRAY_N);

    $json = [
        "event_registration" => json_encode($event_registration_result),
        "event_price_table" => json_encode($event_price_table_result),
        "event_price_item" => json_encode($event_price_item),
    ];

    add_option( "ct_events_plugin_db_dump_{$now}", json_encode($json) );

    $post_id = get_option('ct_events_plugin_page_id');
    wp_delete_post( $post_id);
    delete_option( 'ct_events_plugin_page_id' );

    $db = new DBFactory($wpdb->prefix,$wpdb);
    $db->dropTables();
}

register_deactivation_hook( __FILE__, 'ct_events_deactivate' );
    

/**
 * Loads user's view 
 *
 * @return void
 */
function ct_events_show_plugin()
{
    global $post;
    require_once('event-list.php');
}

add_shortcode( 'ct_events_plugin_shortcode', 'ct_events_show_plugin' );



/**
 * Creates "event" post type 
 *
 * @return void
 */
function events()
{
    
        $labels = array(
            'name'               => _x( 'Events', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Event', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Events', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add New', 'event', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New event', 'your-plugin-textdomain' ),
            'new_item'           => __( 'New event', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit event', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View event', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All events', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search events', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent events:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No events found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No events found in Trash.', 'your-plugin-textdomain' )
        );
    
        $rewrite = array(
            'slug'                       => 'events',
            'with_front'                 => true,
            'hierarchical'               => true,
        );
    
        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => $rewrite,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => true,
            'supports'           => array( 'title', 'editor', 'thumbnail','custom-fields' )
        );
    
        register_post_type( 'event', $args );   
        register_taxonomy_for_object_type( 'category', 'event' );
    
}
        
add_action('init', 'events');


/**
 * Adds meta box in post_type "event" admin page
 *
 * @return void
 */
function date_meta_box() 
{
    add_meta_box( 'meta-box-id', __( 'Date Info', 'textdomain' ), 'meta_box_content', 'event' );

    function meta_box_content(){
        require_once("includes/admin-form.php"); 
    }
}

add_action( 'add_meta_boxes', 'date_meta_box' );


/**
 * Saves event start_date and end_date as custom tags for post_type "events"
 *
 * @param int $post_id
 * @return void
 */
function save_meta_tags( $post_id ) 
{
    
    if(isset($_POST['start_date'])):

        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $concatenated_start_date = $start_date['date'] . ' ' . sprintf("%02d",$start_date['hour']) .':'.sprintf("%02d", $start_date['minute']);
        $concatenated_end_date = $end_date['date'] . ' ' . sprintf("%02d",$end_date['hour']) .':'.sprintf("%02d",$end_date['minute']);

        if(Carbon::createFromFormat('Y-m-d H:i', $concatenated_start_date ) > Carbon::createFromFormat('Y-m-d H:i', $concatenated_end_date )){
            
            $message = "<h2>Invalid date </h2>";
            $message.= "<strong>Start date</strong> cannot be greater than the <strong>end date</strong>.<br/>  ";
            $message.=  "<a href='#' onclick='javascript:(history.back());'>Back</a>";
            wp_die( $message);
            
        }
        $c_start_date = Carbon::createFromFormat('Y-m-d H:i', $concatenated_start_date )->toDateTimeString();
        $c_end_date = Carbon::createFromFormat('Y-m-d H:i', $concatenated_end_date )->toDateTimeString();

        update_post_meta( $post_id, 'start_date', $c_start_date, $prev_value );
        update_post_meta( $post_id, 'end_date', $c_end_date, $prev_value );

    endif;
        
}

add_action( 'save_post', 'save_meta_tags' );



function get_custom_post_type_template($single_template) {
    global $post;

    if ($post->post_type == 'event') {
         $single_template = dirname( __FILE__ ) . '/event-single.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' );