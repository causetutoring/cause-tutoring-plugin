var RegistrationForm = new Vue({
    el: '#registration-form',
    data: {
        form: {},
        url: 'http://localhost/wordpress/wp-content/plugins/registration/',
        step: 1,
        type: null
    },
    methods:{
        postForm:function(){
            var _this = this; 
            console.log(this.form);
            fetch(
                this.url+'registration-store.php',
                {
                    method: 'POST',
                    body: JSON.stringify(_this.form),
                    headers: new Headers({
                        'Content-Type': 'application/json'
                    })
                }
            ).then(function(response){
                return response.body
            }).then(function(result){
                console.log('response',result)
                _this.step = 2;
            })
        }
    }
})