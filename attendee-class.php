<?php

/**
 * Handles information about attendee
 */
class Attendee {

    public $fullname;
    public $wp_post_ID;
    public $email;
    public $association;
    public $faculty;
    public $type;
    public $authorization;
    public $registration_date;
    public $credit;
    protected $wpdb;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    /**
     * Sets Attendee information and stores into a session variable
     *
     * @param array $params
     * @return void
     * @todo check if $_SESSION['attendee'] could be replaced 
     */
    public function set($params)
    {
        $this->fullname = $params['fullname'];
        $this->wp_post_ID = $params['wp_post_ID'];
        $this->email = $params['email'];
        $this->association = $params['association'];
        $this->faculty = $params['faculty'];
        $this->type = $params['type'];
        $this->authorization = $params['authorization'];
        $this->credit = $params['credit'];
        $this->registration_date = $params['registration_date'];
        $_SESSION['attendee'] = $this; 
    }


    /**
     * Returns attendee based in id field
     *
     * @param int $id
     * @return object
     */
    public static function find($id)
    {   
        global $wpdb;
        $results = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}event_registration WHERE id = {$id}", OBJECT );
        return $results;
    }

    /**
     * Returns attendee based in e-mail field
     *
     * @param string $email
     * @return object
     */
    public function findByEmail($email)
    {
        $results = $this->wpdb->get_results( "SELECT * FROM {$this->wpdb->prefix}event_registration WHERE email = '{$email}' AND payment_status <> 'canceled' ", OBJECT );
        return $results;
    }

    /**
     * Saves attendee into database
     *
     * @return void
     */
    public function save()
    {
        $request = [
            'wp_post_ID'        =>  $this->wp_post_ID,
            'fullname'          =>  $this->fullname,
            'email'             =>  $this->email,
            'association'       =>  $this->association,
            'faculty'           =>  $this->faculty,
            'type'              =>  $this->type, 
            'credit'            =>  $this->credit, 
            'payment_status'    =>  'pending', 
            'authorization' =>  ''
        ];

        $this->wpdb->insert( 
            $this->wpdb->prefix.'event_registration', $request        
        );

        return $this->wpdb->insert_id;
    }

    /**
     * Changes payment status on database if paypal returns success 
     *
     * @param int $id
     * @param int $payment_id
     * @return $id
     */
    public function changePaymentStatus($id,$payment_id)
    {        
        return $this->wpdb->update( 
            $this->wpdb->prefix.'event_registration', 
            array( 
                'payment_status' => 'paid',	// string
                'payment_id' => $payment_id,	// string
            ), 
            array( 'id' => $id )
        );
    }

    /**
     * Sets attendee absence to the event 
     *
     * @param int $id
     * @return int $id
     */
    public function cancelPayment($id)
    {
        return $this->wpdb->update( 
            $this->wpdb->prefix.'event_registration', 
            array( 
                'payment_status' => 'canceled',	// string
            ), 
            array( 'id' => $id )
        );
    }

    /**
     * Checks if attendee already paid the event
     *
     * @param int $id
     * @return boolean
     */
    public static function checkIfIsPaid($id)
    {
        global $wpdb;
        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}event_registration WHERE id = '{$id} and payment_status = 1'", OBJECT );
        return ($results); 
    }

    /**
     * Sets attendee session in order to store useful information
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    public static function setAttendeeSession($key, $value)
    {
        $_SESSION['attendeeInfo'][$key] =  $value;
        setcookie('attendeeInfo-'.$key, $value , time() + (86400 * 30), "/");
    }

  

    /**
     * Undocumented function
     * @todo is this really necessary???
     * @return void
     */
    public static function getAttendeeSession()
    {
        return $_SESSION['attendeeInfo'];
    }



}