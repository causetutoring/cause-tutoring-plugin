<?php
    require_once 'event-list-controller.php';
    $events = EventListController::get();
?>

<?php if($events): ?>
    <?php foreach($events as $event): ?>

        <h1>
            <a href="<?=$event->guid?>">
                <?=$event->post_title;?>
            </a>
        </h1>
        <strong>Starts at:</strong> <time datetime="<?=$event->start_date;?>"><?=$event->start_date;?></time><br/>
        <strong>Finishes at:</strong> <time datetime="<?=$event->end_date;?>"><?=$event->end_date;?></time>
        <p><?=$event->post_excerpt;?></p>

    <?php endforeach;?>
<?php else: ?>

    <h5>There are no events scheduled.</h5>

<?php endif; ?>