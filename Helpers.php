<?php

if (!isset($_SESSION)) session_start();

function set_session_flash($name,$value){
  
        $_SESSION[$name] = $value;
  
}
 
function get_session_flash($name){
    if(isset($_SESSION[$name])){
        $value =  $_SESSION[$name];
        unset($_SESSION[$name]);
        return $value;
    }

    return null;
}

function dd($object){
    var_dump($object);exit;
}


/**
 * Sets cookie and hash in order to avoid posts without authorization
 *
 * @return void
 */
function set_protection()
{
    $hash = uniqid();
    $_SESSION['event_plugin_key'] = $hash;
    setcookie('event_plugin_key', $hash , time() + (86400 * 30), "/");
}

/**
 * Returns auth information
 *
 * @param [type] $from
 * @return void
 */
function get_protection($from)
{
    if($from === 'cookie'){
        return $_COOKIE['event_plugin_key'];
    }
    
    if($from === 'session'){
        return $_SESSION['event_plugin_key'];
    }
}

function check_protection(){
    if(get_protection('session') === $_POST['protection']){
        // do nothing
    }else if(get_protection('session') === get_protection('cookie')){
        //
    }else{
        die('You are a such funny guy trying to break our registration plugin!');
    }
}

/**
 * @todo is this really necessary
 *
 * @return void
 */
function destroy_protection()
{
    unset($_SESSION['event_plugin_key']);
    unset ($_COOKIE['event_plugin_key']);
    setcookie("event_plugin_key", "", time()-3600);
}

function serial_now(){
    return date('Y_m_d_H_i_s');
}